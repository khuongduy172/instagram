﻿using Instagram.Models;
using Instagram.Services;
using Xamarin.Forms;

namespace Instagram
{
    public partial class App : Application
    {
        public static Database db = new Database();

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            DependencyService.Register<IMessageService, MessageService>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}