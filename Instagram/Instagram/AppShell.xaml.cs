﻿using Instagram.Views;
using Xamarin.Forms;

namespace Instagram
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(HomePage), typeof(HomePage));
            Routing.RegisterRoute(nameof(SearchPage), typeof(SearchPage));
            Routing.RegisterRoute(nameof(AddPage), typeof(AddPage));
            Routing.RegisterRoute(nameof(NotificationPage), typeof(NotificationPage));
            Routing.RegisterRoute(nameof(ProfilePage), typeof(ProfilePage));
            Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
            Routing.RegisterRoute($"{nameof(LoginPage)}/{nameof(RegisterPage)}", typeof(RegisterPage));
            Routing.RegisterRoute(nameof(StatusPage), typeof(StatusPage));
        }
    }
}