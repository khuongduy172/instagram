﻿using System;
using System.Globalization;
using System.IO;
using Xamarin.Forms;

namespace Instagram.Converters
{
    public class StreamToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ImageSource objImageSource;
            if (value != null)
            {
                objImageSource = ImageSource.FromStream(() => (Stream)value);
            }
            else
            {
                objImageSource = null;
            }
            return objImageSource;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}