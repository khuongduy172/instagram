﻿using Instagram.Models.Dtos;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Instagram.Converters
{
    public class NotiConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var noti = (NotiDto)value;
            if (noti?.TypeNoti == "Follow")
            {
                return $"{noti.FromUser.Name} is following you.";
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}