﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Instagram.Converters
{
    public class FollowStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? "Following" : "Follow";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
