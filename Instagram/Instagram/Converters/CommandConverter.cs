﻿using Instagram.Models;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace Instagram.Converters
{
    public class CommandConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Command OpenProfileCommand = new Command(OpenProfile);
            return OpenProfileCommand;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private async void OpenProfile(object obj)
        {
            var user = (User)obj;
            await Shell.Current.GoToAsync($"ProfilePage?userId={user.Id}");
        }
    }
}