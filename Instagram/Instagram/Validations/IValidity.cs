﻿namespace Instagram.Validations
{
    public interface IValidity
    {
        bool IsValid { get; }
    }
}