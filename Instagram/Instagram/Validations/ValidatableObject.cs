﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.Collections.Generic;
using System.Linq;

namespace Instagram.Validations
{
    public class ValidatableObject<T> : ObservableObject, IValidity
    {
        private IEnumerable<string> _errors;
        private bool _isValid;
        private T _value;

        public List<IValidationRule<T>> Validations { get; } = new List<IValidationRule<T>>();

        public IEnumerable<string> Errors
        {
            get => _errors;
            private set => SetProperty(ref _errors, value);
        }

        public string CurrentError { get; set; }

        public bool IsValid
        {
            get => _isValid;
            private set => SetProperty(ref _isValid, value);
        }

        public T Value
        {
            get
            {
                return _value;
            }
            set
            {
                SetProperty(ref _value, value);
                Validate();
            }
        }

        public ValidatableObject()
        {
            _isValid = true;
            _errors = Enumerable.Empty<string>();
        }

        public bool Validate()
        {
            Errors = Validations
                ?.Where(v => !v.Check(Value))
                ?.Select(v => v.ValidationMessage)
                ?.ToArray()
                ?? Enumerable.Empty<string>();

            IsValid = !Errors.Any();
            CurrentError = Errors.FirstOrDefault();
            OnPropertyChanged("CurrentError");
            OnPropertyChanged("Value");

            return IsValid;
        }
    }
}