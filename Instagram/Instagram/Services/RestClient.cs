﻿using Instagram.Models;
using Instagram.Models.Dtos;
using NativeMedia;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Instagram.Services
{
    public class RestClient
    {
        protected readonly HttpClient _httpClient;

        public RestClient()
        {
            _httpClient = new HttpClient();
            //_httpClient.BaseAddress = new Uri("http://mymoon.somee.com/api/");
            _httpClient.BaseAddress = new Uri("http://192.168.1.3/social/api/");
        }

        private void AttachToken()
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);
        }

        public async Task<AuthResponse> Login(string email, string password)
        {
            var body = new StringContent(JsonConvert.SerializeObject(new { email = email, password = password }), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync("Auth/login", body);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<AuthResponse>(await response.Content.ReadAsStringAsync());
        }

        public async Task Register(string email, string password, string passwordConfirm, string name, bool isMale)
        {
            var body = new StringContent(
                JsonConvert.SerializeObject(
                    new
                    {
                        email = email,
                        password = password,
                        passwordConfirm = passwordConfirm,
                        name = name,
                        isMale = isMale,
                    }),
                Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync("Auth/register", body);
            response.EnsureSuccessStatusCode();
        }

        public async Task<User> GetMe()
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);
            var response = await _httpClient.GetAsync("User/me");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<User>(await response.Content.ReadAsStringAsync());
        }

        public async Task<User> GetUserById(string userId)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);
            var response = await _httpClient.GetAsync($"User/{userId}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<User>(await response.Content.ReadAsStringAsync());
        }

        public async Task CreateStatus(CreateStatusRequest request)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var body = new MultipartFormDataContent();

            body.Add(new StringContent(request.Content), "Content");

            foreach (var file in request.Files)
            {
                body.Add(new StreamContent(await file.MediaFile.OpenReadAsync()), "Files", file.NameWithExtension);
            }

            var response = await _httpClient.PostAsync("Status", body);
            response.EnsureSuccessStatusCode();
        }

        public async Task<ObservableCollection<StatusImage>> GetRandomImages()
        {
            var response = await _httpClient.GetAsync("Status/images/random");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<ObservableCollection<StatusImage>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Status> GetStatusById(string id)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync($"Status/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Status>(await response.Content.ReadAsStringAsync());
        }

        public async Task React(Guid statusId)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync($"Status/{statusId}/react");
            response.EnsureSuccessStatusCode();
        }

        public async Task<PaginationResponse<Status>> GetHomeStatuses()
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync("Status/home");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<PaginationResponse<Status>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<PaginationResponse<Comment>> GetStatusComments(Guid statusId, int pageNumber)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync($"Status/{statusId}/comment?PageNumber={pageNumber.ToString()}&PageSize=5");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<PaginationResponse<Comment>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Comment> AddComment(Guid statusId, string content)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var body = new StringContent(
                JsonConvert.SerializeObject(content),
                Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync($"Status/{statusId}/comment", body);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<Comment>(await response.Content.ReadAsStringAsync());
        }

        public async Task DeleteComment(Guid commentId)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync($"Comment/{commentId}/delete");
            response.EnsureSuccessStatusCode();
        }

        public async Task DeleteStatus(Guid statustId)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync($"Status/{statustId}/delete");
            response.EnsureSuccessStatusCode();
        }

        public async Task<PaginationResponse<StatusImage>> GetUserImages(string userId)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync($"Status/image/{userId}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<PaginationResponse<StatusImage>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<SearchUserDto>> SearchUser(string keyword)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync($"User/search?keyword={keyword}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<SearchUserDto>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<PaginationResponse<NotiDto>> GetNoti()
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var response = await _httpClient.GetAsync($"Noti");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<PaginationResponse<NotiDto>>(await response.Content.ReadAsStringAsync());
        }

        public async Task Follow(Guid userId)
        {
            var token = App.db.ReadUserToken();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);

            var body = new StringContent(
                JsonConvert.SerializeObject(
                    new
                    {
                        userId = userId,
                    }),
                Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync("Follow", body);
            response.EnsureSuccessStatusCode();
        }

        public async Task UnFollow(Guid userId)
        {
            AttachToken();
            var body = new StringContent(
                JsonConvert.SerializeObject(
                    new
                    {
                        userId = userId,
                    }),
                Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync("Follow/unfollow", body);
            response.EnsureSuccessStatusCode();
        }

        public async Task<User> EditProfile(User user)
        {
            var body = new StringContent(
                JsonConvert.SerializeObject(
                    new
                    {
                        name = user.Name,
                        //isMale = user.Gender,
                        bio = user.Bio,
                        dayOfBirth = user.DayOfBirth,
                        phone = user.Phone
                    }),
                Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync("User", body);
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<User>(await response.Content.ReadAsStringAsync());
        }

        public async Task<User> UpdateAvatar(IMediaFile file)
        {
            AttachToken();

            var body = new MultipartFormDataContent();

            body.Add(new StreamContent(await file.OpenReadAsync()), "File", $"{file.NameWithoutExtension}.{file.Extension}");

            var response = await _httpClient.PostAsync("User/avatar", body);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<User>(await response.Content.ReadAsStringAsync());
        }
    }
}