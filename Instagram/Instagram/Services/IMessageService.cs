﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace Instagram.Services
{
    public interface IMessageService
    {
        Task ShowAsync(string title, string message, string cancel);

        Task<bool> ShowAsync(string title, string message, string accept, string cancel);
    }

    public class MessageService : IMessageService
    {
        public async Task ShowAsync(string title, string message, string cancel)
        {
            await Shell.Current.DisplayAlert(title, message, cancel);
        }

        public async Task<bool> ShowAsync(string title, string message, string accept, string cancel)
        {
            return await Shell.Current.DisplayAlert(title, message, accept, cancel);
        }
    }
}