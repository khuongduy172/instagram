﻿using Instagram.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    internal class StatusViewModel : BaseViewModel, IQueryAttributable
    {
        public async void ApplyQueryAttributes(IDictionary<string, string> query)
        {
            var statusId = HttpUtility.UrlDecode(query["statusId"]);
            await LoadData(statusId);
        }

        public Status Status { get; set; } = new Status();

        private async Task LoadData(string id)
        {
            IsLoading = true;
            OnPropertyChanged(nameof(IsLoading));
            try
            {
                Status = await restClient.GetStatusById(id);
                OnPropertyChanged(nameof(Status));
            }
            catch
            {
            }
            finally
            {
                IsLoading = false;
                OnPropertyChanged(nameof(IsLoading));
            }
        }
    }
}