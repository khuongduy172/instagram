﻿using Instagram.Models;
using Instagram.Models.Dtos;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class SearchViewModel : BaseViewModel
    {
        public SearchViewModel()
        {
            SearchCommand = new Command<string>((string keyword) => Search(keyword));
            OpenProfileCommand = new Command(OpenProfile);
        }

        public ICommand SearchCommand { get; set; }
        public Command OpenProfileCommand { get; set; }
        public ObservableCollection<StatusImage> RandomImages { get; set; } = new ObservableCollection<StatusImage>();
        public ObservableCollection<SearchUserDto> SearchResults { get; set; } = new ObservableCollection<SearchUserDto>();
        public bool IsSearch { get; set; } = false;
        private StatusImage _selectedImage;

        public StatusImage SelectedImage
        {
            get { return _selectedImage; }
            set
            {
                _selectedImage = value;
                OpenStatus(_selectedImage.StatusId);
            }
        }

        private async void Search(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                IsSearch = true;
                OnPropertyChanged(nameof(IsSearch));
                var response = await restClient.SearchUser(keyword);
                SearchResults.Clear();
                foreach (var dto in response)
                {
                    SearchResults.Add(dto);
                }
            }
        }

        public override async void OnAppearing()
        {
            base.OnAppearing();
            if (RandomImages.Count <= 0)
            {
                IsLoading = true;
                OnPropertyChanged(nameof(IsLoading));
                try
                {
                    RandomImages = await restClient.GetRandomImages();
                    OnPropertyChanged(nameof(RandomImages));
                }
                catch
                {
                }
                finally
                {
                    IsLoading = false;
                    OnPropertyChanged(nameof(IsLoading));
                }
            }
        }

        private async void OpenStatus(Guid id)
        {
            await Shell.Current.GoToAsync($"StatusPage?statusId={id}");
        }

        private async void OpenProfile(object obj)
        {
            var userId = obj.ToString();
            await Shell.Current.GoToAsync($"ProfilePage?userId={userId}");
        }
    }
}