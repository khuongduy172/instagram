﻿using Instagram.Models;
using Instagram.Models.Dtos;
using System.Collections.ObjectModel;

namespace Instagram.ViewModels
{
    public class NotificationViewModel : BaseViewModel
    {
        public ObservableCollection<NotiDto> Notis { get; set; } = new ObservableCollection<NotiDto>();

        public override async void OnAppearing()
        {
            base.OnAppearing();
            IsLoading = true;
            OnPropertyChanged("IsLoading");
            try
            {
                Notis.Clear();
                var response = await restClient.GetNoti();
                foreach (var item in response.Data)
                {
                    Notis.Add(item);
                }
            }
            catch
            {
            }
            finally
            {
                IsLoading = false;
                OnPropertyChanged("IsLoading");
            }
        }
    }
}
