﻿using Instagram.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class HomeViewModel : BaseViewModel, IQueryAttributable
    {
        public HomeViewModel()
        {
            RefreshCommand = new Command(OnRefresh);
            AddPageCommand = new Command(NavigateToAddPage);
        }

        public ObservableCollection<Status> Statuses { get; set; } = new ObservableCollection<Status>();
        public Command RefreshCommand { get; set; }
        public Command AddPageCommand { get; set; }
        public bool IsRefresh { get; set; } = false;

        public async void NavigateToAddPage()
        {
            await Shell.Current.GoToAsync("//AddPage");
        }

        public async void OnRefresh()
        {
            IsRefresh = true;
            OnPropertyChanged(nameof(IsRefresh));
            try
            {
                var response = await restClient.GetHomeStatuses();
                Statuses.Clear();
                foreach (var status in response.Data)
                {
                    Statuses.Add(status);
                }
            }
            catch
            {
            }
            finally
            {
                IsRefresh = false;
                OnPropertyChanged(nameof(IsRefresh));
            }
        }

        public override async void OnAppearing()
        {
            base.OnAppearing();
            if (Statuses.Count <= 0)
            {
                IsLoading = true;
                OnPropertyChanged("IsLoading");
                try
                {
                    var response = await restClient.GetHomeStatuses();
                    foreach (var status in response.Data)
                    {
                        Statuses.Add(status);
                    }
                }
                catch
                {
                }
                finally
                {
                    IsLoading = false;
                    OnPropertyChanged("IsLoading");
                }
            }
        }

        public void ApplyQueryAttributes(IDictionary<string, string> query)
        {
            try
            {
                var deletedId = HttpUtility.UrlDecode(query["deletedId"]);
                var status = Statuses.FirstOrDefault(s => s.Id.ToString() == deletedId);
                if (status != null)
                {
                    Statuses.Remove(status);
                }
            }
            catch
            {
            }
        }
    }
}