﻿using Instagram.Models.Dtos;
using Instagram.Views.Components;
using NativeMedia;
using Rg.Plugins.Popup.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class AddViewModel : BaseViewModel
    {
        public Command PickCommand { get; }
        public Command CaptureCommand { get; }
        public Command BackCommand { get; }
        public Command PostCommand { get; }
        public CreateStatusRequest Request { get; set; } = new CreateStatusRequest();

        public bool IsShowAddButton { get; set; } = true;

        public AddViewModel()
        {
            PickCommand = new Command(PickImage);
            BackCommand = new Command(OnBack);
            PostCommand = new Command(OnPost);
            CaptureCommand = new Command(CaptureImage);
        }

        private async void PickImage()
        {
            var result = await MediaGallery.PickAsync(10, MediaFileType.Image);

            if (result.Files == null)
            {
                return;
            }

            foreach (var file in result.Files)
            {
                Request.Files.Add(new FileDto
                {
                    NameWithExtension = $"{file.NameWithoutExtension}.{file.Extension}",
                    ContentType = file.ContentType,
                    Content = await file.OpenReadAsync(),
                    MediaFile = file,
                });
            }
            IsShowAddButton = false;
            OnPropertyChanged(nameof(IsShowAddButton));
            OnPropertyChanged(nameof(Request));
        }

        private async void CaptureImage()
        {
            if (!MediaGallery.CheckCapturePhotoSupport())
                return;

            var status = await Permissions.RequestAsync<Permissions.Camera>();

            if (status != PermissionStatus.Granted)
                return;

            var result = await MediaGallery.CapturePhotoAsync();

            Request.Files.Add(new FileDto
            {
                NameWithExtension = $"{result.NameWithoutExtension}.{result.Extension}",
                ContentType = result.ContentType,
                Content = await result.OpenReadAsync(),
                MediaFile = result,
            });
            IsShowAddButton = false;
            OnPropertyChanged(nameof(IsShowAddButton));
            OnPropertyChanged(nameof(Request));
        }

        private void OnBack()
        {
            IsShowAddButton = true;
            Request.Files.Clear();
            OnPropertyChanged(nameof(IsShowAddButton));
        }

        private async void OnPost()
        {
            await PopupNavigation.Instance.PushAsync(new LoadingPopup());
            try
            {
                await restClient.CreateStatus(Request);
                await Shell.Current.GoToAsync("//HomePage");
            }
            catch
            {
            }
            finally
            {
                await PopupNavigation.Instance?.PopAsync();
            }
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}