﻿using Instagram.Models;
using Instagram.Services;
using Instagram.Views.Components;
using NativeMedia;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class ProfileViewModel : BaseViewModel, IQueryAttributable
    {
        private readonly IMessageService _messageService;

        public ProfileViewModel()
        {
            LogoutCommand = new Command(OnLogoutClicked);
            EditCommand = new Command(EditProfile);
            FollowCommand = new Command(Follow);
            UnFollowCommand = new Command(UnFollow);
            RefreshCommand = new Command(OnRefresh);
            ChangeAvatarCommand = new Command(ChangeAvatar);
            _messageService = DependencyService.Get<IMessageService>();
        }

        public User UserData { get; set; }
        public string UserId { get; set; } = string.Empty;
        public bool IsNewUser { get; set; } = true;
        public ObservableCollection<StatusImage> Images { get; set; } = new ObservableCollection<StatusImage>();

        public Command LogoutCommand { get; }
        public Command EditCommand { get; set; }
        public Command FollowCommand { get; set; }
        public Command UnFollowCommand { get; set; }
        public Command RefreshCommand { get; set; }
        public Command ChangeAvatarCommand { get; set; }
        public bool IsRefresh { get; set; } = false;

        private StatusImage _selectedImage;

        public StatusImage SelectedImage
        {
            get { return _selectedImage; }
            set
            {
                _selectedImage = value;
                OpenStatus(_selectedImage.StatusId);
            }
        }

        private async void ChangeAvatar(object obj)
        {
            var result = await MediaGallery.PickAsync(1, MediaFileType.Image);
            try
            {
                UserData = await restClient.UpdateAvatar(result.Files.FirstOrDefault());
                OnPropertyChanged(nameof(UserData));
            }
            catch
            {
                await _messageService.ShowAsync("Error", "Error updating avatar!", "Ok");
            }
        }

        public async void OnRefresh()
        {
            IsRefresh = true;
            OnPropertyChanged(nameof(IsRefresh));
            try
            {
                if (string.IsNullOrEmpty(UserId))
                {
                    UserData = await restClient.GetMe();
                }
                else
                {
                    UserData = await restClient.GetUserById(UserId);
                }
                var response = await restClient.GetUserImages(UserData.Id.ToString());
                Images.Clear();
                foreach (var image in response.Data)
                {
                    Images.Add(image);
                }
                OnPropertyChanged("UserData");
            }
            catch
            {
            }
            finally
            {
                IsRefresh = false;
                OnPropertyChanged(nameof(IsRefresh));
            }
        }

        private async void EditProfile(object obj)
        {
            var user = (User)obj;
            await PopupNavigation.Instance.PushAsync(new EditPopup(user));
        }

        private async void Follow(object obj)
        {
            var user = (User)obj;
            try
            {
                await restClient.Follow(user.Id);
                UserData.IsFollowing = true;
                OnPropertyChanged(nameof(UserData));
            }
            catch
            {
            }
        }

        private async void UnFollow(object obj)
        {
            var user = (User)obj;
            try
            {
                await restClient.UnFollow(user.Id);
                UserData.IsFollowing = false;
                OnPropertyChanged(nameof(UserData));
            }
            catch
            {
            }
        }

        private async void OpenStatus(Guid id)
        {
            await Shell.Current.GoToAsync($"StatusPage?statusId={id}");
        }

        private async void OnLogoutClicked(object obj)
        {
            App.db.DeleteUsertoken();
            IsNewUser = true;
            await Shell.Current.GoToAsync("//LoginPage");
        }

        private async Task LoadData()
        {
            if (IsNewUser)
            {
                IsLoading = true;
                OnPropertyChanged("IsLoading");
                try
                {
                    if (string.IsNullOrEmpty(UserId))
                    {
                        UserData = await restClient.GetMe();
                    }
                    else
                    {
                        UserData = await restClient.GetUserById(UserId);
                    }
                    var response = await restClient.GetUserImages(UserData.Id.ToString());
                    Images.Clear();
                    foreach (var image in response.Data)
                    {
                        Images.Add(image);
                    }
                    OnPropertyChanged("UserData");
                }
                catch
                {
                }
                finally
                {
                    IsLoading = false;
                    OnPropertyChanged("IsLoading");
                    IsNewUser = false;
                }
            }
        }

        public override async void OnAppearing()
        {
            base.OnAppearing();
            await LoadData();
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();
            UserId = string.Empty;
        }

        public void ApplyQueryAttributes(IDictionary<string, string> query)
        {
            var userId = HttpUtility.UrlDecode(query["userId"]);
            UserId = userId;
            IsNewUser = true;
        }
    }
}