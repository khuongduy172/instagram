﻿using Instagram.Validations;
using Instagram.Views.Components;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        public RegisterViewModel()
        {
            RegisterCommand = new Command(OnRegisterClicked);
            AddValidations();
        }

        private string _passwordConfirm;
        public Command RegisterCommand { get; }
        public ValidatableObject<string> Email { get; set; } = new ValidatableObject<string>();
        public ValidatableObject<string> Password { get; set; } = new ValidatableObject<string>();

        public string PasswordConfirm
        {
            get => _passwordConfirm;
            set
            {
                _passwordConfirm = value;
                if (Password != null && Password.Value != value)
                {
                    PasswordConfirmError = "Password confirm does not match.";
                }
                else
                {
                    PasswordConfirmError = null;
                }
                OnPropertyChanged("PasswordConfirmError");
            }
        }

        public string PasswordConfirmError { get; set; }
        public ValidatableObject<string> Name { get; set; } = new ValidatableObject<string>();
        public ValidatableObject<bool> IsMale { get; set; } = new ValidatableObject<bool>();

        private async void OnRegisterClicked(object obj)
        {
            if (Validate())
            {
                await PopupNavigation.Instance.PushAsync(new LoadingPopup());
                try
                {
                    await restClient.Register(Email.Value, Password.Value, PasswordConfirm, Name.Value, IsMale.Value);
                    await Shell.Current.GoToAsync("//LoginPage");
                }
                catch
                {
                    await PopupNavigation.Instance.PopAsync();
                }
                finally
                {
                    await PopupNavigation.Instance.PopAsync();
                }
            }
        }

        private void AddValidations()
        {
            Email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Email is required." });
            Email.Validations.Add(new EmailRule<string> { ValidationMessage = "Invalid email." });
            Password.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Password is required." });
            Password.Validations.Add(new IsValidPasswordRule<string> { ValidationMessage = "Password must between 8-20 digits, one upper case character, one lower case character, one number, and one special character." });
            Name.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Name is required." });
        }

        private bool Validate()
        {
            bool isValidEmail = Email.Validate();
            bool isValidPassword = Password.Validate();
            bool isValidName = Name.Validate();
            return isValidEmail && isValidPassword && isValidName && string.IsNullOrEmpty(PasswordConfirmError);
        }
    }
}