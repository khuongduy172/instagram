﻿using Instagram.Services;
using Instagram.Validations;
using Instagram.Views.Components;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IMessageService _messageService;

        public Command LoginCommand { get; }
        public Command ForgotPasswordNavCommand { get; }
        public Command RegisterNavCommand { get; }
        public ValidatableObject<string> Email { get; set; } = new ValidatableObject<string>();
        public ValidatableObject<string> Password { get; set; } = new ValidatableObject<string>();

        public LoginViewModel()
        {
            LoginCommand = new Command(OnLoginClicked);
            ForgotPasswordNavCommand = new Command(OnForgotClicked);
            RegisterNavCommand = new Command(OnRegisterClicked);
            _messageService = DependencyService.Get<IMessageService>();
            AddValidations();
        }

        private async void OnLoginClicked(object obj)
        {
            if (Validate())
            {
                await PopupNavigation.Instance.PushAsync(new LoadingPopup());
                try
                {
                    var test = await restClient.Login(Email.Value, Password.Value);
                    App.db.SetUserToken(new Models.UserToken() { Token = test.Token, ExpiredAt = test.Expires });
                    await Shell.Current.GoToAsync("//HomePage");
                }
                catch
                {
                    await _messageService.ShowAsync("Error", "Wrong email/password!", "Ok");
                }
                finally
                {
                    await PopupNavigation.Instance.PopAsync();
                }
            }
        }

        private void AddValidations()
        {
            Email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Email is required." });
            Email.Validations.Add(new EmailRule<string> { ValidationMessage = "Invalid email." });
            Password.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Password is required." });
        }

        private bool Validate()
        {
            bool isValidEmail = Email.Validate();
            bool isValidPassword = Password.Validate();
            return isValidEmail && isValidPassword;
        }

        private async void OnForgotClicked(object obj)
        {
            await Shell.Current.GoToAsync("//LoginPage/RegisterPage");
        }

        private async void OnRegisterClicked(object obj)
        {
            await Shell.Current.GoToAsync("//LoginPage/RegisterPage");
        }
    }
}