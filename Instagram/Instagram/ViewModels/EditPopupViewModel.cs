﻿using Instagram.Models;
using Instagram.Services;
using Instagram.Views.Components;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class EditPopupViewModel : BaseViewModel
    {
        private readonly IMessageService _messageService;
        public EditPopupViewModel(User user)
        {
            User = user;
            _messageService = DependencyService.Get<IMessageService>();
            SaveCommand = new Command(Save);
        }

        public User User { get; set; }
        public Command SaveCommand { get; set; }

        private async void Save(object obj)
        {
            await PopupNavigation.Instance.PushAsync(new LoadingPopup());
            try
            {
                await restClient.EditProfile(User);
                await PopupNavigation.Instance.PopAsync();
                await PopupNavigation.Instance.PopAsync();
            }
            catch
            {
                await PopupNavigation.Instance.PopAsync();
                await _messageService.ShowAsync("Error", "Something went wrong!", "Ok");
            }
        }
    }
}