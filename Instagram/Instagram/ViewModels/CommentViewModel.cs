﻿using Instagram.Models;
using Instagram.Services;
using Instagram.Views.Components;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class CommentViewModel : BaseViewModel
    {
        private readonly IMessageService _messageService;

        public CommentViewModel(Guid statusId, int commentCount)
        {
            StatusId = statusId;
            CommentCount = commentCount;
            CommentCommand = new Command(Comment);
            GetMoreCommand = new Command(GetMoreComment);
            DeleteCommand = new Command(Delete);
            _messageService = DependencyService.Get<IMessageService>();
        }

        public Guid StatusId { get; set; }
        public int CommentCount { get; set; }
        public string Content { get; set; }
        public bool HasNextPage { get; set; } = false;
        public int CurrentPage { get; set; } = 1;
        public ObservableCollection<Comment> Comments { get; set; } = new ObservableCollection<Comment>();
        public Command CommentCommand { get; set; }
        public Command GetMoreCommand { get; set; }
        public Command DeleteCommand { get; set; }

        private async void Delete(object obj)
        {
            var comment = (Comment)obj;
            if (comment.IsOwner)
            {
                if (await _messageService.ShowAsync("Alert", "Do you want to delete this comment?", "Yes", "No"))
                {
                    await restClient.DeleteComment(comment.Id);
                    Comments.Remove(comment);
                }
            }
            else
            {
                await _messageService.ShowAsync("Warning", "You can't take this action!", "Ok");
            }
        }

        public async void Comment()
        {
            if (!string.IsNullOrEmpty(Content))
            {
                await PopupNavigation.Instance.PushAsync(new LoadingPopup());
                var comment = await restClient.AddComment(StatusId, Content);
                var temp = new ObservableCollection<Comment>(Comments.ToList());
                Comments.Clear();
                Comments.Add(comment);
                foreach (var dto in temp)
                {
                    Comments.Add(dto);
                }
                Content = string.Empty;
                OnPropertyChanged(nameof(Content));
                await PopupNavigation.Instance?.PopAsync();
            }
        }

        public async void GetMoreComment()
        {
            try
            {
                CurrentPage = CurrentPage + 1;
                var response = await restClient.GetStatusComments(StatusId, CurrentPage);
                HasNextPage = response.HasNextPage;
                OnPropertyChanged(nameof(HasNextPage));
                foreach (var comment in response.Data)
                {
                    Comments.Add(comment);
                }
            }
            catch
            {
            }
        }

        public override async void OnAppearing()
        {
            base.OnAppearing();

            if (CommentCount > 0)
            {
                IsLoading = true;
                OnPropertyChanged("IsLoading");
                try
                {
                    var response = await restClient.GetStatusComments(StatusId, CurrentPage);
                    HasNextPage = response.HasNextPage;
                    OnPropertyChanged(nameof(HasNextPage));
                    foreach (var comment in response.Data)
                    {
                        Comments.Add(comment);
                    }
                }
                catch
                {
                }
                finally
                {
                    IsLoading = false;
                    OnPropertyChanged("IsLoading");
                }
            }
        }

        public override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}