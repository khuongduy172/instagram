﻿using Instagram.Services;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Instagram.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public static RestClient restClient = new RestClient();
        public HubConnection hubConnection = new HubConnectionBuilder().WithUrl("http://192.168.1.4/social/corehub").Build();

        public event PropertyChangedEventHandler PropertyChanged;

        public static async Task GotoHomePage(Guid deletedId)
        {
            await Shell.Current.GoToAsync($"//HomePage?deletedId={deletedId}");
        }

        public bool IsLoading { get; set; } = false;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public async Task Connect()
        {
            await hubConnection.StartAsync();
        }

        public async Task Disconnect()
        {
            await hubConnection.StopAsync();
        }

        public async Task JoinRoom(string roomName)
        {
            await hubConnection.InvokeAsync(nameof(JoinRoom), roomName);
        }

        public virtual async void OnAppearing()
        {
            if (App.db.ReadUserToken() == null || App.db.ReadUserToken().ExpiredAt < System.DateTime.UtcNow)
            {
                await Shell.Current.GoToAsync("//LoginPage");
            }
        }

        public virtual void OnDisappearing()
        { }
    }
}