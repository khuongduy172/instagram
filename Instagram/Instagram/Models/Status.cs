﻿using Instagram.Services;
using Instagram.ViewModels;
using Instagram.Views.Components;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Linq;
using Xamarin.Forms;

namespace Instagram.Models
{
    public class Status : INotifyPropertyChanged
    {
        private readonly IMessageService _messageService;
        public Status()
        {
            ReactCommand = new Command(React);
            OpenCommentCommand = new Command(OpenComment);
            DeleteCommand = new Command(Delete);
            _messageService = DependencyService.Get<IMessageService>();
        }

        public Guid Id { get; set; }
        public string Content { get; set; }
        public Guid OwnerId { get; set; }
        public bool IsReacted { get; set; }
        public bool IsOwner { get; set; }
        public int ReactCount { get; set; }
        public int CommentCount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public List<StatusImage> StatusImages { get; set; }
        public User Owner { get; set; }
        public Command ReactCommand { get; set; }
        public Command OpenCommentCommand { get; set; }
        public Command DeleteCommand { get; set; }

        private async void React(object obj)
        {
            var status = (Status)obj;
            await BaseViewModel.restClient.React(status.Id);
            IsReacted = !IsReacted;
            OnPropertyChanged(nameof(IsReacted));
        }

        private async void OpenComment(object obj)
        {
            var status = (Status)obj;
            await PopupNavigation.Instance.PushAsync(new CommentPopup(status.Id, status.CommentCount));
        }

        private async void Delete(object obj)
        {
            var status = (Status)obj;
            if (status.IsOwner)
            {
                if (await _messageService.ShowAsync("Alert", "Do you want to delete this post?", "Yes", "No"))
                {
                    await BaseViewModel.restClient.DeleteStatus(status.Id);
                    await BaseViewModel.GotoHomePage(status.Id);
                }
            }
            else
            {
                await _messageService.ShowAsync("Warning", "You can't take this action!", "Ok");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}