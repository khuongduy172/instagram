﻿using System.Collections.Generic;
using System;
using SQLite;
using System.Linq;

namespace Instagram.Models
{
    public class Database
    {
        private readonly SQLiteConnection db;
        public Database()
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            db = new SQLiteConnection(System.IO.Path.Combine(folder, "LocalDb.db3"));
            db.CreateTable<UserToken>();
        }
        public bool SetUserToken(UserToken token)
        {
            try
            {
                db.Insert(token);
                return true;
            }
            catch
            {
                return false;
                throw;
            }
        }
        public UserToken ReadUserToken()
        {
            try
            {
                return db.Table<UserToken>().OrderBy(u => u.ExpiredAt).LastOrDefault();
            }
            catch
            {
                return null;
                throw;
            }
        }
        public bool DeleteUsertoken()
        {
            try
            {
                var token = ReadUserToken();
                db.Delete(token);
                return true;
            }
            catch
            {
                return false;
                throw;
            }
        }
    }
}
