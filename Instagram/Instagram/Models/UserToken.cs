﻿using SQLite;
using System;

namespace Instagram.Models
{
    public class UserToken
    {
        [PrimaryKey]
        public string Token { get; set; }

        public DateTime ExpiredAt { get; set; }
    }
}