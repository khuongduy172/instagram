﻿using System;

namespace Instagram.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Bio { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public DateTime DayOfBirth { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public int PostCount { get; set; }
        public int FollowerCount { get; set; }
        public int FollowingCount { get; set; }
        public bool IsOwner { get; set; }
        public bool IsFollowing { get; set; }
    }
}