﻿using System;

namespace Instagram.Models.Dtos
{
    public class NotiDto
    {
        public string TypeNoti { get; set; }
        public Guid OwnerId { get; set; }
        public Guid? FromId { get; set; }
        public User FromUser { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}