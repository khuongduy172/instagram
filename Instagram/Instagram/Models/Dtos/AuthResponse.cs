﻿using System;

namespace Instagram.Models.Dtos
{
    public class AuthResponse
    {
        public bool Auth{ get; set; }
        public string Token { get; set; }
        public DateTime Expires { get; set; }
    }
}
