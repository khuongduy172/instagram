﻿using NativeMedia;
using System.Collections.ObjectModel;
using System.IO;

namespace Instagram.Models.Dtos
{
    public class CreateStatusRequest
    {
        public string Content { get; set; } = string.Empty;
        public ObservableCollection<FileDto> Files { get; set; } = new ObservableCollection<FileDto>();
    }

    public class FileDto
    {
        public string NameWithExtension { get; set; }
        public string ContentType { get; set; }
        public Stream Content { get; set; }
        public IMediaFile MediaFile { get; set; }
    }
}