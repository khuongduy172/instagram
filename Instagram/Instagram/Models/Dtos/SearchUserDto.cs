﻿using System;

namespace Instagram.Models.Dtos
{
    public class SearchUserDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public bool IsFollowing { get; set; }
    }
}
