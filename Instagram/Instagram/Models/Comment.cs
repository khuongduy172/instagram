﻿using Instagram.Services;
using Instagram.ViewModels;
using System;
using Xamarin.Forms;

namespace Instagram.Models
{
    public class Comment
    {
        public Guid Id { get; set; }
        public Guid StatusId { get; set; }
        public Guid OwnerId { get; set; }
        public string Content { get; set; }
        public bool IsOwner { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public User Owner { get; set; }
    }
}