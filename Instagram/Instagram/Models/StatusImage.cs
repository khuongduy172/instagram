﻿using System;

namespace Instagram.Models
{
    public class StatusImage
    {
        public string Name { get; set; }
        public Guid StatusId { get; set; }
        public string Url { get; set; }
    }
}