﻿using Instagram.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Instagram.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StatusPage : ContentPage
    {
        public StatusPage()
        {
            InitializeComponent();
            BindingContext = new StatusViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            (BindingContext as BaseViewModel).OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            (BindingContext as BaseViewModel).OnDisappearing();
        }
    }
}