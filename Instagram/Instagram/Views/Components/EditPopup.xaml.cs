﻿using Instagram.Models;
using Instagram.ViewModels;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace Instagram.Views.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditPopup : PopupPage
    {
        public EditPopup()
        {
            InitializeComponent();
        }

        public EditPopup(User user)
        {
            InitializeComponent();
            BindingContext = new EditPopupViewModel(user);
        }
    }
}