﻿using Instagram.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using Xamarin.Forms.Xaml;

namespace Instagram.Views.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CommentPopup : PopupPage
    {
        public CommentPopup()
        {
            InitializeComponent();
        }

        public CommentPopup(Guid statusId, int commentCount)
        {
            InitializeComponent();
            BindingContext = new CommentViewModel(statusId, commentCount);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            (BindingContext as BaseViewModel).OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            (BindingContext as BaseViewModel).OnDisappearing();
        }
    }
}