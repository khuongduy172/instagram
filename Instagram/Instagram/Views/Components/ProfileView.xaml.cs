﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Instagram.Views.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileView : ContentView
    {
        public ProfileView()
        {
            InitializeComponent();
        }
    }
}